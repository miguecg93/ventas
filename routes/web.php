<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductosController;
use App\Http\Controllers\ComprasController;
use App\Http\Controllers\FacturasController;
use App\Http\Controllers\HomeController;
use GuzzleHttp\Middleware;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});
/*
Route::get('/empleados', function () {
    return view('empleados.index');
});

Route::get('empleados/create', [EmpleadoController::class, 'create']);
*/


Route::resource('productos', ProductosController::class)->middleware('auth');
Route::resource('compras', ComprasController::class)->middleware('auth');
Route::resource('facturas', FacturasController::class)->middleware('auth');

//para eliminar vistas o cosas de la ruta es asi
//Auth::routes(['register'=>false,'reset'=>false]);
Auth::routes();

Route::get('/home', [ProductosController::class, 'index'])->name('home');



    Route::group(['middleware' => 'auth'], function () {
        Route::get('/', [HomeController::class, 'index']);
    });

