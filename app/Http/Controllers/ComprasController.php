<?php

namespace App\Http\Controllers;

use App\Models\Compras;
use App\Models\Productos;
use Illuminate\Http\Request;
use DB;
class ComprasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $productos = Productos::all();

    return View('compras.index', compact('productos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        //validacion de campos
        $campos = [
            'producto' => 'required',
            'cantidad' => 'required|max:200',
        ];

        $mensaje = [
            'producto.required'=>'El Producto es requerido',
            'cantidad.required'=>'La Cantidad del producto es requerida'

        ];

        $this->validate($request, $campos, $mensaje);
        //FIN VALIDACION DE CAMPOS


        $datoscompras = request()->except('_token');
        $user = auth()->user()->id;

        DB::insert('insert into compras(id_usuario,producto,cantidad,factura, facturado,created_at, updated_at) values (?,?,?,?,?,?,?)', [$user, $request['producto'], $request['cantidad'], '0', '0', date("Y-m-d H:i:s", strtotime('now')),date('Y-m-d H:i:s')]);
        //Compras::insert($datosempleados);
        //return response()->json($datosempleados);

        return redirect('compras')->with('mensaje', 'Compra realizada con exito.');

    }

    public function buscarproductos(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Compras  $compras
     * @return \Illuminate\Http\Response
     */
    public function show(Compras $compras)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Compras  $compras
     * @return \Illuminate\Http\Response
     */
    public function edit(Compras $compras)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Compras  $compras
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Compras $compras)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Compras  $compras
     * @return \Illuminate\Http\Response
     */
    public function destroy(Compras $compras)
    {
        //
    }
}
