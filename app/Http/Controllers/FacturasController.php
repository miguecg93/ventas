<?php

namespace App\Http\Controllers;

use App\Models\Facturas;
use App\Models\Compras;
use App\Models\Productos;
use Illuminate\Http\Request;
use DB;

class FacturasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datos['facturas'] = Facturas::paginate(10);
        $datos2['facturas'] = DB::select('select fs.*, us.name from facturas fs inner join users us on fs.id_usuario = us.id ');
        return view('facturas.index', $datos2);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $cantidadtotal = 0;
        $montototalconimpuesto = 0;
        $montototalsinimpuesto = 0;
        $users =
        DB::select('select DISTINCT id_usuario from compras where facturado = 0');

        foreach ($users as $user) {

            $productos = DB::select('select DISTINCT producto from compras where id_usuario = ?', [$user->id_usuario]);

            foreach ($productos as $producto) {

                $compras = DB::select('select * from compras where id_usuario = ? and producto = ?', [$user->id_usuario, $producto->producto]);



                foreach ($compras as $compra) {
                    $precios = DB::select('select precio,impuesto from productos where id = ?', [$compra->producto]);

                    foreach ($precios as $precio) {
                        $precio2 = $precio->precio;
                        $impuesto2 = $precio->impuesto;
                    }

                    $cantidad = $compra->cantidad;
                    $cero = 0;
                    $montoconimpuesto = ($precio2 * $cantidad);

                    $montosinimpuesto = ($precio2 * $impuesto2) / 100 * ($cantidad);


                    $cantidadtotal = ($cantidadtotal + $cantidad);
                    $montototalconimpuesto = ($montototalconimpuesto + $montoconimpuesto);
                    $montototalsinimpuesto = ($montototalsinimpuesto + $montosinimpuesto);


                   // DB::update('update compras SET facturado = 1 Where id = ?', [$compra->id]);

                }


            }

            DB::insert('insert into facturas(id_usuario,monto,montoimpuesto) values (?,?,?)', [$user->id_usuario, $montototalconimpuesto, $montototalsinimpuesto]);
            DB::update('update compras SET factura = (select id from facturas order by id desc LIMIT 1) , facturado = 1 Where id_usuario = ?', [$user->id_usuario]);
        }

        return redirect('facturas');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Facturas  $facturas
     * @return \Illuminate\Http\Response
     */
    public function show(Facturas $facturas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Facturas  $facturas
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $facturas = Facturas::findorfail($id);
        $compras = DB::select('select cs.id, cs.id_usuario, cs.producto, cs.cantidad, cs.factura, cs.facturado, cs.created_at, cs.updated_at,
        ps.id, ps.nombre, ps.precio, ps.impuesto, us.name from compras cs
       INNER JOIN productos ps ON cs.producto = ps.id
       INNER JOIN users us ON cs.id_usuario = us.id where factura = ?', [$id]);




        return view('facturas.edit', compact('facturas'), compact('compras'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Facturas  $facturas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Facturas $facturas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Facturas  $facturas
     * @return \Illuminate\Http\Response
     */
    public function destroy(Facturas $facturas)
    {
        //
    }
}
