<?php

namespace App\Http\Controllers;

use App\Models\Productos;
use Illuminate\Http\Request;

class ProductosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datos['productos'] = Productos::paginate(3);
        return view('productos.index', $datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('productos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         //validacion de campos
         $campos = [
            'nombre' => 'required|string|max:200',
            'precio' => 'required|max:10',
            'impuesto' => 'required|max:3',
        ];

        $mensaje = [
            'name.required'=>'El Nombre es requerido',
            'precio.required'=>'El Precio es requerido',
            'impuesto.required'=>'El Impuesto es requerido',
        ];

        $this->validate($request, $campos, $mensaje);
        //FIN VALIDACION DE CAMPOS


        $datosproductos = request()->except('_token');

        Productos::insert($datosproductos);
        //return response()->json($datosempleados);

        return redirect('productos')->with('mensaje', 'Producto agregado con exito.');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function show(Productos $productos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $productos = Productos::findorfail($id);
        return view('productos.edit', compact('productos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         //validacion de campos
         $campos = [
            'nombre' => 'required|string|max:200',
            'precio' => 'required|max:10',
            'impuesto' => 'required|max:3',
        ];

        $mensaje = [
            'name.required'=>'El Nombre es requerido',
            'precio.required'=>'El Precio es requerido',
            'impuesto.required'=>'El Impuesto es requerido',
        ];

        $this->validate($request, $campos, $mensaje);
        //FIN VALIDACION DE CAMPOS


    //
    $datosproductos = request()->except('_token','_method');


    Productos::where('id','=',$id)->update($datosproductos);

    $productos = Productos::findorfail($id);

   // return view('empleados.edit', compact('empleado'))->with('mensaje', 'Empleado actualizado con exito.');
   return redirect('productos')->with('mensaje', 'Producto actualizado con exito.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $productos = Productos::findorfail($id);

        Productos::destroy($id);



        return redirect('productos')->with('mensaje2', 'Producto eliminado con exito.');
    }
}
