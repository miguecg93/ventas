<?php

namespace App\Http\Controllers;

use App\Models\Compras;
use App\Models\Productos;
use App\Models\User;
use Illuminate\Http\Request;
use DB;
use PDO;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $user = auth()->user()->id;
        $user_types =
        DB::select('select * from users where id = ?', [$user]);
foreach($user_types as $user_type)
{
            $results = $user_type->user_type;
}

        if($results == 1)
        {
            return redirect('facturas');
        }
        else
        {

            return redirect('compras');
        }

    }
}
