@extends('layouts.app')

@section('content')
<div class="container">

    @if(Session::has('mensaje'))
    <div class="alert alert-success alert-dismissible" role="alert">
            {{ Session::get('mensaje') }}
    </div>
    @endif

    @if(Session::has('mensaje2'))
    <div class="alert alert-danger alert-dismissible" role="alert">
            {{ Session::get('mensaje2') }}

    </div>
    @endif

MOSTRAR LISTA DE FACTURAS


<br>
<br>
<form action=" {{ url('/facturas') }}" method="post" class="d-inline" >
<input type="submit" value="Generar Facturas" class="btn btn-success">
@csrf
</form>
<a href="{{ url('productos') }}" class="btn btn-success">Productos</a>
<br>
<br>


<table class="table table-light">
    <thead class="thead-light">
        <tr>
            <th>Nr</th>
            <th>Usuario</th>
            <th>Monto</th>
            <th>Monto Impuesto</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($facturas as $factura)
        <tr>
            <td> {{ $factura->id }} </td>
            <td> {{ $factura->name }} </td>
            <td> {{ $factura->monto }} </td>
            <td> {{ $factura->montoimpuesto }} </td>
            <td>
                <a href=" {{ url('/facturas/'.$factura->id.'/edit') }}" class="btn btn-primary">
                    Ver Factura
                </a>


                  </td>
        </tr>
        @endforeach
    </tbody>

</table>

</div>

@endsection
