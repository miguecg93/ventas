@if(Session::has('mensaje'))
{{ Session::get('mensaje') }}
@endif

@extends('layouts.app')

@section('content')


<h1>Factura Nro: {{ $facturas->id }}</h1>

@if(count($errors)>0)


<div class="alert alert-danger" role="alert">
    <ul>
        @foreach( $errors->all() as $error)
         <li>   {{ $error }}</li>
        @endforeach
    </ul>
</div>

@endif



<table class="table table-light">
    <thead class="thead-light">
        <tr>
            <th>Producto</th>
            <th>Cantidad</th>
            <th>Monto</th>
            <th>Monto Impuesto</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($compras as $compra)
        <tr>
            <td> {{ $compra->nombre }} </td>
            <td> {{ $compra->cantidad }}  </td>
            <td> {{ ($compra->precio) * ($compra->cantidad) }} </td>
            <td> {{ (($compra->precio * $compra->impuesto) / 100 * ($compra->cantidad)) }} </td>

        </tr>
        @endforeach
    </tbody>

</table>

<h2>Monto Con Impuesto: {{ $facturas->monto }}</h2>
<h2>Impuesto: {{ $facturas->montoimpuesto }}</h2>
<a href="{{ url('facturas') }}"  class="btn btn-danger">Volver
</a>

@endsection

