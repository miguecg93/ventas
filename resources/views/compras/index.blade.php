@extends('layouts.app')

@section('content')

@if(Session::has('mensaje'))
<div class="alert alert-success alert-dismissible" role="alert">
        {{ Session::get('mensaje') }}
</div>
@endif



<div class="container">

formulario para Comprar Productos

@if(count($errors)>0)


<div class="alert alert-danger" role="alert">
    <ul>
        @foreach( $errors->all() as $error)
         <li>   {{ $error }}</li>
        @endforeach
    </ul>
</div>

@endif

<form action=" {{ url('/compras') }}" method="post">
    @csrf
<div class="row">

    <div class="col-4">
        <div class="form-group">

                <div class="col-3">
                    <br>
                    <label for="producto" class="form-check-label text-muted">Producto:</label>
                </div>
                <div class="col-6">
                    <select name="producto" id="producto" class="form-control">
                        <option value=""> Seleccione Producto </option>
                        @foreach($productos as $producto)
                            <option value=" {{ $producto->id }}"> {{ ucfirst($producto->nombre)}} </option>
                        @endforeach
                    </select>


            </div>
        </div>
    </div>

    <div class="col-6">
        <div class="form-group">

                <div class="col-3">
                    <br>
                    <label for="cantidad" class="form-check-label text-muted">Cantidad:</label>
                </div>
                <div class="col-4">
                    <input type="number" name='cantidad' id='cantidad' class="form-control form-control-lg" value="">
            </div>
        </div>
    </div>




</div>

<br>
<br>



<input type="submit" value="Comprar" class="btn btn-success">

</form>



</div>
@endsection
