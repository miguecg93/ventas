@if(Session::has('mensaje'))
{{ Session::get('mensaje') }}
@endif

@extends('layouts.app')

@section('content')
<div class="container">

formulario para editar Productos


<form action="{{ url('/productos/'.$productos->id) }}" method="post">
@csrf
{{ method_field('PATCH') }}
@include('productos.form',['modo'=>'Editar'])

</form>
</div>
@endsection
