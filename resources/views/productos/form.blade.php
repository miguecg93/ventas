<h1> {{ $modo }} Producto</h1>

@if(count($errors)>0)


<div class="alert alert-danger" role="alert">
    <ul>
        @foreach( $errors->all() as $error)
         <li>   {{ $error }}</li>
        @endforeach
    </ul>
</div>

@endif


<div class="row">

    <div class="col-6">
        <div class="form-group">

                <div class="col-3">
                    <br>
                    <label for="nombre" class="form-check-label text-muted">Nombre:</label>
                </div>
                <div class="col-4">
                    <input type="text" name='nombre' id='nombre' class="form-control form-control-lg" value="{{ isset($productos->nombre)?$productos->nombre:old('nombre') }}">
            </div>
        </div>
    </div>

    <div class="col-6">
        <div class="form-group">

                <div class="col-3">
                    <br>
                    <label for="precio" class="form-check-label text-muted">Precio:</label>
                </div>
                <div class="col-4">
                    <input type="text" name='precio' id='precio' class="form-control form-control-lg" value="{{ isset($productos->precio)?$productos->precio:old('precio') }}">
            </div>
        </div>
    </div>


    <div class="col-6">
        <div class="form-group">

                <div class="col-3">
                    <br>
                    <label for="impuesto" class="form-check-label text-muted">Impuesto %:</label>
                </div>
                <div class="col-4">
                    <input type="text" name='impuesto' id='impuesto'  class="form-control form-control-lg" value="{{ isset($productos->impuesto)?$productos->impuesto:old('impuesto') }}">
            </div>
        </div>
    </div>


</div>

<br>
<br>

<div class="row">
<div class="col-12">

<input type="submit" value="{{ $modo }}" class="btn btn-success">

<a href="{{ url('productos') }}"  class="btn btn-danger">Volver
</a>

</div>
</div>

