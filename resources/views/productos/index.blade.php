
@extends('layouts.app')

@section('content')
<div class="container">

    @if(Session::has('mensaje'))
    <div class="alert alert-success alert-dismissible" role="alert">
            {{ Session::get('mensaje') }}
    </div>
    @endif

    @if(Session::has('mensaje2'))
    <div class="alert alert-danger alert-dismissible" role="alert">
            {{ Session::get('mensaje2') }}

    </div>
    @endif

MOSTRAR LISTA DE PRODUCTOS


<br>
<br>
<a href="{{ url('productos/create') }}" class="btn btn-success">Crear Producto</a>
<a href="{{ url('facturas') }}" class="btn btn-success">Facturas</a>
<br>
<br>


<table class="table table-light">
    <thead class="thead-light">
        <tr>
            <th>Nr</th>
            <th>Nombre</th>
            <th>Precio</th>
            <th>Impuesto</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($productos as $producto)
        <tr>
            <td> {{ $producto->id }} </td>
            <td> {{ $producto->nombre }} </td>
            <td> {{ $producto->precio }} </td>
            <td> {{ $producto->impuesto }} </td>
            <td>
                <a href=" {{ url('/productos/'.$producto->id.'/edit') }}" class="btn btn-primary">
                    Editar
                </a>


                |

                <form action=" {{ url('/productos/'.$producto->id) }}" class="d-inline" method="post">
                    @csrf
                    {{ method_field('DELETE') }}
                    <input type="submit" class="btn btn-danger" onclick="return confirm('¿Realmente Quieres Borrar Este Producto?')"
                    value="Borrar">
                </form>

                  </td>
        </tr>
        @endforeach
    </tbody>

</table>
{!! $productos->links() !!}
</div>

@endsection
