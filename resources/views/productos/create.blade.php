@if(Session::has('mensaje'))
{{ Session::get('mensaje') }}
@endif

@extends('layouts.app')

@section('content')
<div class="container">

formulario para crear Productos

<form action=" {{ url('/productos') }}" method="post">
    @csrf
    @include('productos.form',['modo'=>'Guardar'])
</form>

</div>
@endsection
